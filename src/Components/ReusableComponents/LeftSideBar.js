import React from 'react'
import { useHistory } from 'react-router-dom'
import '../ReusableComponentsCss/LeftSideBar.css'

const LeftSideBar = (props) => {

    const history = useHistory()

    const logout = () => {
        localStorage.removeItem('token')
        history.push('/login')
    }

    return (
        <div className="left-side-bar">
            <button onClick={() => props.changeActiveButton(1)}
                className={props.active === 1 ? "active" : null}>Patient</button>

            <button onClick={() => props.changeActiveButton(2)}
                className={props.active === 2 ? "active" : null}>Survey</button>
                
            <button onClick={logout}>Logout</button>
        </div>
    )
}

export default LeftSideBar
