import React from 'react'
import '../ReusableComponentsCss/NavigationBar.css'

const NavigationBar = (props) => {
    return (
        <nav>
            {
                props.active === 1 ?
                    <button onClick={() => props.changeCreatePaient()}>Create Patient</button> :
                    <button onClick={() => props.changeCreateSurvey()}>Create Survey</button>
            }
        </nav>
    )
}

export default NavigationBar
