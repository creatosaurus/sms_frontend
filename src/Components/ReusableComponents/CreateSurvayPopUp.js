import React, { useState } from 'react'
import '../ReusableComponentsCss/CreatePatientPopUp.css'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import axios from 'axios'

const CreateSurvayPopUp = (props) => {

    const [survey, setsurvey] = useState("")
    const [loading, setloading] = useState(false)
    const [questions, setquestions] = useState([{
        question: "",
        stop: null
    }])

    const editQuestion = (value, indexToChange) => {
        let changedData = questions.map((data, index) => {
            if (index === indexToChange) {
                data.question = value
            }
            return data
        })

        setquestions(changedData)
    }

    const editExit = (value, indexToChange) => {
        let changedData = questions.map((data, index) => {
            if (index === indexToChange) {
                data.stop = value
            }
            return data
        })

        setquestions(changedData)
    }

    const createQuestion = () => {
        setquestions((prev) => [...prev, {
            question: "",
            stop: null
        }])
    }

    const createSurvey = async () => {
        try {
            const token = localStorage.getItem('token')
            const decoded = jwt_decode(token);
            setloading(true)
            const res = await axios.post(constant.URL + "survey/create", {
                hospitalId: decoded.id,
                name: survey,
                questions: questions,
                totalQuestions: questions.length
            })
            setloading(false)
            setsurvey("")
            setquestions([{
                question: "",
                stop: null
            }])
            props.addSurvey(res.data.data)
        } catch (error) {
            setloading(false)
        }
    }

    return (
        <div className="patient-container-popup">
            <div className="card card1">
                <h4>Create Survey</h4>
                <div className="wrapper">
                    <span>Survey Name</span>
                    <input type="text"
                        value={survey}
                        onChange={(e) => setsurvey(e.target.value)}
                        placeholder="enter survey name" />
                </div>
                {
                    questions.map((data, index) => {
                        return <React.Fragment key={index}>
                            <div className="wrapper">
                                <span>Question {index + 1}</span>
                                <textarea type="text"
                                    value={data.question}
                                    onChange={(e) => editQuestion(e.target.value, index)}
                                    placeholder="create question" />
                            </div>

                            <div className="wrapper">
                                <div>
                                    <input value={data.stop}
                                        onChange={(e) => editExit(e.target.value, index)}
                                        className="exit-no"
                                        type="number"
                                        placeholder="no" />
                                    <span style={{ marginLeft: 15 }}>Enter the number to exit the survey</span>
                                </div>
                            </div>
                        </React.Fragment>
                    })
                }

                <button className="create-more" onClick={createQuestion}>Create Question</button>

                <div className="button-container bottom">
                    <button onClick={() => props.changeCreateSurvey()}>Cancel</button>
                    <button onClick={loading ? null : createSurvey}>{loading ? "Creating ..." : "Create"}</button>
                </div>
            </div>
        </div>
    )
}

export default CreateSurvayPopUp
