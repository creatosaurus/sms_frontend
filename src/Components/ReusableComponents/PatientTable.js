import React from 'react'
import '../ReusableComponentsCss/PatientTable.css'
import Pencil from '../../Assets/pencil.png'
import Trash from '../../Assets/trash.png'

const PatientTable = (props) => {
    return (
        <div className="patient-container">
            <table>
                <tbody>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    {
                        props.patients.map((data, index) => {
                            return <tr key={data._id}>
                                <td>{index + 1}</td>
                                <td>{data.name}</td>
                                <td>{data.mobile}</td>
                                <td>{data.email}</td>
                                <td style={{ cursor: 'pointer' }}><img src={Pencil} alt="" /></td>
                                <td style={{ cursor: 'pointer' }}><img src={Trash} alt="" /></td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default PatientTable
