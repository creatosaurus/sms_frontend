import React from 'react'
import '../ReusableComponentsCss/PatientTable.css'
import Pencil from '../../Assets/pencil.png'
import Trash from '../../Assets/trash.png'
import View from '../../Assets/view.png'

const SurveyTable = (props) => {
    return (
        <div className="patient-container">
            <table>
                <tbody>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Total Questions</th>
                        <th>Status</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    {
                        props.survey.map((data, index) => {
                            return <tr key={data._id}>
                                <td>{index + 1}</td>
                                <td>{data.name}</td>
                                <td>{data.totalQuestions}</td>
                                <td>{data.startSurvey ? "Active" : "Inactive"}</td>
                                <td style={{ cursor: 'pointer' }}><img src={View} alt="" /></td>
                                <td style={{ cursor: 'pointer' }}><img src={Pencil} alt="" /></td>
                                <td style={{ cursor: 'pointer' }}><img src={Trash} alt="" /></td>
                            </tr>
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default SurveyTable
