import React, { useState, useEffect } from 'react'
import '../PagesCss/Login.css'
import axios from 'axios'
import constant from '../../constant'

const Onboarding = (props) => {

    useEffect(() => {
        if (props.location.state === undefined) return props.history.push('/signup') // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const [name, setname] = useState("")
    const [contact, setcontact] = useState("")
    const [address, setaddress] = useState("")
    const [loading, setloading] = useState(false)

    const submit = async () => {
        try {
            if (name === "") return alert("Please enter hospital name")
            if (contact === "") return alert("Please enter contact number")
            if (address === "") return alert("Please enter address")
            setloading(true)
            const res = await axios.post(constant.URL + "hospital/signup", {
                email: props.location.state.email,
                name: name,
                password: props.location.state.password,
                mobile: contact,
                address: address
            })
            setloading(false)
            localStorage.setItem('token', res.data.token);
            props.history.push('/')
        } catch (error) {
            alert(error.response.data.error)
            setloading(false)
        }
    }

    return (
        <div className="login-container">
            <div className="card" style={{ height: 430 }}>
                <div className="wrapper">
                    <div>Hospital Name</div>
                    <input type="text"
                        value={name}
                        onChange={(e) => setname(e.target.value)}
                        placeholder="enter hospital name" />
                </div>

                <div className="wrapper">
                    <div>Contact Number</div>
                    <input type="number"
                        value={contact}
                        onChange={(e) => setcontact(e.target.value)}
                        placeholder="enter hospital contact number" />
                </div>

                <div className="wrapper">
                    <div>Address</div>
                    <textarea type="test"
                        value={address}
                        onChange={(e) => setaddress(e.target.value)}
                        placeholder="enter hospital address" />
                </div>

                <button onClick={loading ? null : submit}>{loading ? "Submitting ..." : "Submit"}</button>
            </div>
        </div>
    )
}

export default Onboarding
