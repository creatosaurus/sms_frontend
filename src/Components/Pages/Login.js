import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import '../PagesCss/Login.css'
import constant from '../../constant'

const Login = (props) => {

    const [email, setemail] = useState("")
    const [password, setpassword] = useState("")
    const [loading, setloading] = useState(false)

    const login = async () => {
        try {
            if(email === "") return alert("please enter the valid email")
            if(password === "") return alert("please enter the valid email")
            setloading(true)
            const res = await axios.post(constant.URL + "hospital/login", {
                email: email,
                password: password
            })
            setloading(false)
            localStorage.setItem("token", res.data.token)
            props.history.push('/')
        } catch (error) {
            setloading(false)
            alert(error.response.data.error)
        }
    }

    return (
        <div className="login-container">
            <div className="card">
                <div className="wrapper">
                    <div>Email</div>
                    <input type="email"
                        value={email}
                        onChange={(e) => setemail(e.target.value)}
                        placeholder="enter your email" />
                </div>

                <div className="wrapper">
                    <div>Password</div>
                    <input type="password"
                        value={password}
                        onChange={(e) => setpassword(e.target.value)}
                        placeholder="enter your password" />
                </div>

                <Link to="/signup">Signup</Link>
                <button onClick={loading ? null : login}>{loading ? "Logging you in ..." : "Login"}</button>
            </div>
        </div>
    )
}

export default Login
