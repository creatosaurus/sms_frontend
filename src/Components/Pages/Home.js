import React, { useState, useEffect } from 'react'
import '../PagesCss/Home.css'
import CreatePatientPopUp from '../ReusableComponents/CreatePatientPopUp'
import LeftSideBar from '../ReusableComponents/LeftSideBar'
import NavigationBar from '../ReusableComponents/NavigationBar'
import PatientTable from '../ReusableComponents/PatientTable'
import SurveyTable from '../ReusableComponents/SurveyTable'
import axios from 'axios'
import constant from '../../constant'
import jwt_decode from "jwt-decode";
import CreateSurvayPopUp from '../ReusableComponents/CreateSurvayPopUp'

const Home = () => {

    const [activeLeftButton, setactiveLeftButton] = useState(1)
    const [createPatient, setcreatePatient] = useState(false)
    const [createSurvey, setcreateSurvey] = useState(false)
    const [patients, setpatients] = useState([])
    const [survey, setsurvey] = useState([])

    useEffect(() => {
        getPatient()
        getSurvey()
    }, [])

    const getPatient = async () => {
        try {
            const token = localStorage.getItem('token')
            const decoded = jwt_decode(token);
            const res = await axios.get(constant.URL + "patient/" + decoded.id)
            setpatients(res.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    const getSurvey = async () => {
        try {
            const token = localStorage.getItem('token')
            const decoded = jwt_decode(token);
            const res = await axios.get(constant.URL + "survey/" + decoded.id)
            setsurvey(res.data.data)
        } catch (error) {
            console.log(error.response.data)
        }
    }

    const changeActiveButton = (value) => {
        setactiveLeftButton(value)
    }

    const changeCreatePaient = () => {
        setcreatePatient((prev) => !prev)
    }

    const changeCreateSurvey = () => {
        setcreateSurvey((prev) => !prev)
    }

    const addPatient = (data) => {
        setpatients((prev) => [data, ...prev])
    }

    const addSurvey = (data) => {
        setsurvey((prev) => [data, ...prev])
    }

    return (
        <div className="home-container">
            {createPatient ? <CreatePatientPopUp changeCreatePaient={changeCreatePaient} addPatient={addPatient} /> : null}
            {createSurvey ? <CreateSurvayPopUp changeCreateSurvey={changeCreateSurvey} addSurvey={addSurvey} /> : null}
            <NavigationBar active={activeLeftButton} changeCreatePaient={changeCreatePaient} changeCreateSurvey={changeCreateSurvey} />
            <main>
                <LeftSideBar active={activeLeftButton} changeActiveButton={changeActiveButton} />
                <section>
                    {
                        activeLeftButton === 1 ?  <PatientTable patients={patients} /> : <SurveyTable survey={survey} />
                    }
                </section>
            </main>
        </div>
    )
}

export default Home
