import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import '../PagesCss/Login.css'

const Signup = (props) => {

    const [email, setemail] = useState("")
    const [password, setpassword] = useState("")

    const submit = () => {
        if (email === "") return alert("Please enter email address")
        if (password === "") return alert("Please enter password")
        if (!new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/).test(email)) return alert("Please enter valid email address")
        if (password.length < 8) return alert("Password must be 8 characters long")
        props.history.push({
            pathname: '/onboarding',
            state: {
                email: email.trim(),
                password: password.trim()
            }
        })
    }

    return (
        <div className="login-container">
            <div className="card">
                <div className="wrapper">
                    <div>Email</div>
                    <input type="email"
                        placeholder="enter your email"
                        value={email}
                        onChange={(e) => setemail(e.target.value)} />
                </div>

                <div className="wrapper">
                    <div>Password</div>
                    <input type="password"
                        placeholder="enter your password"
                        value={password}
                        onChange={(e) => setpassword(e.target.value)} />
                </div>
                <Link to="/login">Login</Link>
                <button onClick={submit}>Signup</button>
            </div>
        </div>
    )
}

export default Signup
