import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Login from './Components/Pages/Login'
import Onboarding from './Components/Pages/Onboarding'
import Signup from './Components/Pages/SignUp'
import Home from './Components/Pages/Home'
import NotFound from './Components/Pages/NotFound'
import ProtectedRoutes from './Components/Pages/ProtectedRoutes'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/onboarding" component={Onboarding} />
        <ProtectedRoutes exact path="/" component={Home} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
